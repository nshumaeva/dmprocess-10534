-- m_LOANAPPLPROCLOG_TRAN__PLALLOCDRIVER_M_V2_PROC
with 
CTT as (select 
  CR.COSTTYPE_DK,
  CT.CCODE as COSTTYPE_CCODE,
  CR.COSTTYPECLASS_DK,
  CC.CCODE as COSTTYPECLASS_CCODE,
  CR.DATE_FLAG,
  CR.DAY_AMOUNT,
  CR.SIGN,
  CR.ANALYSISGRP_DRIVER_UK,
  CR.COSTTYPEALG_DK as ALGORITНM_COSTTYPE_DK,
  ALG.CCODE as ALG_CCODE
  from DMPR.COSTTYPERULE_SHIST CR
  inner join DMPR.COSTTYPEALG_SDIM ALG on cr.COSTTYPEALG_DK = alg.DK
     and alg.deleted_flag = 'N' and alg.default_flag = 'N' 
  left join DMPR.COSTTYPE_SDIM CT on cr.COSTTYPE_dK = ct.dk
     and ct.deleted_flag = 'N' and ct.default_flag = 'N' 
  left join DMPR.COSTTYPECLASS_SDIM CC on cr.COSTTYPECLASS_dK = cc.dk
     and cc.deleted_flag = 'N' and cc.default_flag = 'N' 
group by
  CR.COSTTYPE_DK,
  CT.CCODE,
  CR.COSTTYPECLASS_DK,
  CC.CCODE,
  CR.DATE_FLAG,
  CR.DAY_AMOUNT,
  CR.SIGN,
  CR.ANALYSISGRP_DRIVER_UK,
  CR.COSTTYPEALG_DK,
  ALG.CCODE 
),
V1 as
(
  select /*+ use_hash(src dc ct)*/
    src.VALUE_DAY,
    src.ANALYSISGRP_DRIVER_UK,
	src.MARKER  
    src.DEAL_UK,
    src.CLIENT_UK,
    src.CLIENTSEGMENT_UK,
    src.PRODUCT_UK,
    case
        when marker = 1 and coalesce(CTT1.COSTTYPE_DK, CTT2.COSTTYPE_DK, CTT3.COSTTYPE_DK, 0) = 'ATTTRACTION'
        then ct.DK
        else coalesce(CTT1.COSTTYPE_DK, CTT2.COSTTYPE_DK, CTT3.COSTTYPE_DK, 0) 
    end as COSTTYPE_DK,
    case
        when marker = 1 and coalesce(CTT1.COSTTYPE_CCODE, CTT2.COSTTYPE_CCODE, CTT3.COSTTYPE_CCODE, '0') = 
            (select DK from DMPR.COSTTYPE_SDIM where CCODE = 'ATTTRACTION')
        then ct.CCODE
        else coalesce(CTT1.COSTTYPE_CCODE, CTT2.COSTTYPE_CCODE, CTT3.COSTTYPE_CCODE, '0')
    end as COSTTYPE_CCODE,
    coalesce(CTT1.COSTTYPE_CCODE, CTT2.COSTTYPE_CCODE, CTT3.COSTTYPE_CCODE, '0') as COSTTYPE_CCODE,
    coalesce(CTT1.COSTTYPECLASS_DK, CTT2.COSTTYPECLASS_DK, CTT3.COSTTYPECLASS_DK, 0) as COSTTYPECLASS_DK,
    coalesce(CTT1.COSTTYPECLASS_CCODE, CTT2.COSTTYPECLASS_CCODE, CTT3.COSTTYPECLASS_CCODE, 'N/A') as COSTTYPECLASS_CCODE,
    src.WORKTIME,
    row_number() OVER (PARTITION BY src.CLIENT_UK, src.ANALYSISGRP_DRIVER_UK order by src.CLIENT_UK) rn,
    COUNT(1) as CNT,
    src.ADDRREF_CITY_UK,
    src.PRIMARYBANK_FLAG,
    src.SALARY_FLAG
  
  from
  (
  select /*+ use_hash(log m st f l lrb de deal p2 cl2 fp cls cr csf)*/
    log.VALUE_DAY,
    f.ANALYSISGRP_DRIVER_UK,
	case
        when f.SOURCE_CCODE = 'LOANAPPL_O' 
        then 1 -- вторичные продажи
        when f.SOURCE_CCODE = 'LOANAPPL_M' 
        then 2 -- ипотека
        else 3
    end as MARKER,
    lrb.DEAL_UK,
    COALESCE(l.CLIENT_UK,CL.UK, cl2.UK, 0) as CLIENT_UK,
    CLIENTSEGMENT_GRACE_UK as CLIENTSEGMENT_UK,
    case when nvl(deal.product_uk,-1)> 0 then deal.product_uk
    when nvl(l.product_uk,-1) > 0 then l.product_uk
    else p2.uk
    end as PRODUCT_UK,
	nvl(V1.PRODUCT_UK,0) as PRODUCT_LOW_UK
    abs(nvl(l.start_date, to_date((CASE WHEN LENGTH(TRIM(APPSRC_CCODE)) = 15 THEN '20'||substr(APPSRC_CCODE, 10, 6) ELSE '59991231' END), 'YYYYMMDD')) - nvl(FP.FIRSTACCOUNT_DATE, to_date('59991231', 'yyyymmdd'))) as min_start_date,
    log.end_time - log.start_time as WORKTIME,
    ADDRREF_UK as ADDRREF_CITY_UK,
    PRIMARYBANK_OUT_FLAG as PRIMARYBANK_FLAG,
    SALARY_FLAG,
	V1. MARKER,
    DE.START_DATE AS DE_START_DATE,
 lrb.CHANNEL_UK
  from dmpr.LOANAPPLPROCLOG_TRAN log
  join dmpr.MODULE_LDIM m
    on m.uk=log.module_uk
        and m.ccode='CCM'
        and m.default_flag = 'N'
        and m.deleted_flag = 'N'
  join dmpr.PROCSRCSTATUS_SDIM st
    on st.uk=LOG.PROCSRCSTATUS_FINISH_UK
        and st.deleted_flag = 'N'
        and st.CCODE in ('CCM~VERIFY')
  join dmpr.COVSKILLFILTER_SHIST f 
    on log.PROCSRCACTION_UK = f.PROCSRCACTION_UK
        and f.SOURCE_CCODE IN ('LOANAPPL', 'LOANAPPL_O', 'LOANAPPL_M')
        and last_day(log.VALUE_DAY) between f.EFFECTIVE_FROM and f.EFFECTIVE_TO
        and f.DELETED_FLAG = 'N'
        and f.ANALYSISGRP_DRIVER_UK in (select UK from dmpr.ANALYSISGRP_SDIM where CCODE in ('VERIFYHARD', 'VERIFYSOFT', 'VERIFYOTH'))
  join dmpr.PROCSRCACTION_SDIM ac
    on ac.uk = log.PROCSRCACTION_UK
		and ac.deleted_flag = 'N'      
  left join dmpr.LOANAPPL_SDIM l
    on l.uk = log.loanappl_uk
        and l.deleted_flag = 'N'
        and l.default_flag = 'N'
        and log.value_day between l.start_date  and l.end_date-1
  left join dmpr.LOANAPPLRB_SDIM lrb
    on lrb.uk = log.loanappl_uk
        and lrb.deleted_flag = 'N'
        and lrb.default_flag = 'N'
  left join dmpr.DEALLOAN_EDIM de
    on de.uk=lrb.deal_uk
        and nvl(lrb.deal_uk,0) > 0
        and de.effective_from <= log.value_day
        and de.effective_to > log.value_day
  left join dmpr.DEAL_EDIM deal
    on lrb.DEAL_UK = deal.uk
        and deal.deleted_flag = 'N'
        and deal.default_flag = 'N'
        and log.value_day between deal.effective_from and deal.effective_to -1
  left join dmpr.PRODUCT_EDIM p2
    on trim(p2.ccode) = substr(log.appsrc_ccode,3,4)
        and log.value_day between p2.effective_from and p2.effective_to-1
        and p2.deleted_flag = 'N'
  left join dmpr.client_edim cl
	on to_date((CASE WHEN f.SOURCE_CCODE = 'LOANAPPL_O' and substr (APPSRC_CCODE, 1, 3) = 'SV0' THEN '20'||substr(APPSRC_CCODE, 9, 6)  ELSE '59991231' END), 'YYYYMMDD') between cl.effective_from and cl.effective_to-1
		and cl.PIN = CASE WHEN f.SOURCE_CCODE = 'LOANAPPL_O' and substr(APPSRC_CCODE, 1, 3) = 'SV0' THEN substr(APPSRC_CCODE, 15, 6) ELSE NULL END
		and cl.deleted_flag ='N'
		and cl.default_flag ='N'
  left join dmpr.client_edim cl2
    on to_date((CASE WHEN LENGTH(TRIM(APPSRC_CCODE)) = 15 THEN '20'||substr(APPSRC_CCODE, 10, 6) ELSE '59991231' END), 'YYYYMMDD') between cl2.effective_from  and cl2.effective_to -1
        and cl2.PIN = CASE WHEN LENGTH(TRIM(APPSRC_CCODE)) = 15 THEN substr(APPSRC_CCODE, 4, 6) ELSE NULL END
        and cl2.deleted_flag = 'N'
        and cl2.default_flag = 'N'
  left join DMPR.CLIENTFIRSTPRODUCT_LSTAT FP
    on FP.CLIENT_UK = coalesce(l.client_uk,сl.uk,cl2.uk)
  left join dmpr.CLIENTPROFILE_SSTAT cls
    on cls.client_uk= coalesce(l.client_uk,cl.uk, cl2.uk)
        and last_day(to_date('$$P_END_DATE','yyyymmdd'))= cls.value_day
        and cls.deleted_flag='N'
        and cls.adjreporttype_uk = 2
  left join dmpr.CLIENTREGION_SHIST cr
    on coalesce(l.client_uk,сl.uk, cl2.uk) = cr.client_uk
        and cr.effective_from <= log.value_day
        and cr.effective_to > log.value_day
        and cr.deleted_flag = 'N'
  left join dmpr.CLIENTSALARYFLAG_SHIST csf
    on coalesce(l.client_uk,сl.uk, cl2.uk) = csf.client_uk
        and csf.effective_from <= to_date('$$P_END_DATE', 'yyyymmdd')
        and csf.effective_to > to_date('$$P_END_DATE', 'yyyymmdd')
        and csf.deleted_flag = 'N'
    where log.value_day between to_date('$$P_START_DATE','yyyymmdd') and to_date('$$P_END_DATE','yyyymmdd') 
  ) src
  left join 
  (
    select /*+ no_merge*/
      dc.deal_uk,
      dc.effective_from,
      dc.effective_to,
      dc.CHANNEL_UK
    from dmpr.DEAL2CHANNEL_SHIST dc
      where dc.deleted_flag='N'
        and dc.channeltype_uk in (SELECT /*+ materialize*/ uk FROM dmpr.CHANNELTYPE_LDIM WHERE ccode='01')
  ) dc
    on dc.deal_uk=src.deal_uk
        and src.DE_START_DATE >= dc.effective_from
        and src.DE_START_DATE < dc.effective_to
  left join CTT ctt1 on src.ANALYSISGRP_DRIVER_UK = ctt1. analysisgrp_driver_uk and ctt1.ALG_CCODE = 'FIRST_PRODUCT' 
          AND  abs(nvl(src.min_start_date,0)) >nvl(ctt1.DAY_AMOUNT, 0) and nvl(ctt1.sign, -2)='>'
  left join CTT ctt2 on src.ANALYSISGRP_DRIVER_UK = ctt2. analysisgrp_driver_uk and ctt2.ALG_CCODE = 'FIRST_PRODUCT' 
          AND  abs(nvl(src.min_start_date,0)) <=nvl(ctt2.DAY_AMOUNT, 0) and nvl(ctt2.sign, -2)='<='
  left join CTT ctt3 on src.ANALYSISGRP_DRIVER_UK = ctt3. analysisgrp_driver_uk and ctt3.ALG_CCODE = 'FIX' AND  nvl(ctt3.date_flag, 'N') ='N'
        

  group by 
    src.VALUE_DAY,
    src.ANALYSISGRP_DRIVER_UK, 
    src.DEAL_UK,
    src.CLIENT_UK,
    src.CLIENTSEGMENT_UK,
    src.PRODUCT_UK,
    coalesce(src.CHANNEL_UK, dc.CHANNEL_UK, 0),
    case
        when marker = 1 and coalesce(CTT1.COSTTYPE_DK, CTT2.COSTTYPE_DK, CTT3.COSTTYPE_DK, 0) = 'ATTTRACTION'
        then ct.DK
        else coalesce(CTT1.COSTTYPE_DK, CTT2.COSTTYPE_DK, CTT3.COSTTYPE_DK, 0) 
    end,
    case
        when marker = 1 and coalesce(CTT1.COSTTYPE_CCODE, CTT2.COSTTYPE_CCODE, CTT3.COSTTYPE_CCODE, '0') = 
            (select DK from DMPR.COSTTYPE_SDIM where CCODE = 'ATTTRACTION')
        then ct.CCODE
        else coalesce(CTT1.COSTTYPE_CCODE, CTT2.COSTTYPE_CCODE, CTT3.COSTTYPE_CCODE, '0')
    end,
    coalesce(CTT1.COSTTYPECLASS_DK, CTT2.COSTTYPECLASS_DK, CTT3.COSTTYPECLASS_DK, 0),
    coalesce(CTT1.COSTTYPECLASS_CCODE, CTT2.COSTTYPECLASS_CCODE, CTT3.COSTTYPECLASS_CCODE, 'N/A'),
    src.WORKTIME,
    src.ADDRREF_CITY_UK,
    src.PRIMARYBANK_FLAG,
    src.SALARY_FLAG
)
  select
    to_date('$$P_END_DATE', 'yyyymmdd') as VALUE_DAY,
    0 as ANALYSISGRP_PC_UK,
    V1.ANALYSISGRP_DRIVER_UK,
    V1.DEAL_UK,
    0 as STAFFING_UK,
    0 as PROFITCENTER_UK,
    V1.CLIENT_UK,
    V1.CLIENTSEGMENT_UK,
    nvl(pp.UK,0) as PRODUCT_UK,
    nvl(V1.CHANNEL_UK,0) as CHANNEL_UK,
    V1.COSTTYPE_DK,
    V1.COSTTYPE_CCODE,
    sum(V1.WORKTIME) as WORKTIME,
	V1.marker
    case when client_uk <= 0 then sum(V1.CNT)
    when V1.client_uk > 0 and V1.rn = 1 then COUNT (distinct V1.value_day) else 0 
    end as UNQ_CL_CNT,
    sum(V1.CNT) as CNT,
    V1.ADDRREF_CITY_UK,
    V1.PRIMARYBANK_FLAG as PRIMARYBANK_FLAG,
    'N' as ATM_FLAG,
    14 as PARTITION_NUMBER,
    V1.SALARY_FLAG,
    V1.COSTTYPECLASS_DK,
    V1.COSTTYPECLASS_CCODE
  from V1
left join DMPR.PRODUCT_EDIM p on p.uk = V1.PRODUCT_UK AND p.effective_to > to_date('$$P_END_DATE', 'yyyymmdd') 
                           AND p.effective_from <= to_date('$$P_END_DATE', 'yyyymmdd') AND p.deleted_flag = 'N'
         
left join DMPR.PRODUCT_EDIM pp on pp.uk = p.PARENT_UK AND pp.effective_to > to_date('$$P_END_DATE', 'yyyymmdd')
                           AND pp.effective_from <= to_date('$$P_END_DATE', 'yyyymmdd') AND pp.deleted_flag = 'N'
         
  group by
    V1.ANALYSISGRP_DRIVER_UK,
    V1.DEAL_UK,
    V1.CLIENT_UK,
    V1.CLIENTSEGMENT_UK,
    nvl(pp.UK,0),
	nvl(V1.PRODUCT_UK,0), 
    nvl(V1.CHANNEL_UK,0),
    V1.COSTTYPE_DK,
    V1.COSTTYPE_CCODE,
    V1.COSTTYPECLASS_DK,
    V1.COSTTYPECLASS_CCODE,
    V1.ADDRREF_CITY_UK,
    V1.PRIMARYBANK_FLAG,
    V1.SALARY_FLAG,
    V1.RN,--_mixed 003A731943258592_--
	V1.marker